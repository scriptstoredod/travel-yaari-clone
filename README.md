Every business wish to be one or other way the same and if you want to start a site just as same as “Travelyaari.com”, then you have reached the right place. Our Travel Yaari Clone Script has all the relevant features and benefits that could result in bringing a hike to your business career.


PRODUCT DESCRIPTION
UNIQUE FEATURES:
Agent Registration              
100% Live Bus Ticket Inventory  
Bus Ticket Cancellation      
Reserved Bus seats for Ladies
My Bookings                      
No Hassle with Bus Travel Agents
Refund Status                    
Benefit more with Coupons
Print/SMS Tickets              
Privilege Card

USER PANEL:
⦁ Customer login credentials along with forgot password option.
⦁ Personal account details
⦁ Email notification
⦁ Search option enabled for source city and destination city
⦁ Check on available bus details, select desired seat, and boarding point
⦁ Send the booked ticket details via SMS
⦁ Send the booked ticket details via Email
⦁ Can view en-numbers of bus snaps and videos
⦁ User Wallet available
⦁ Payment Gateway enabled
⦁ Print tickets
⦁ Check refund status by entering the ticket number
BUS ADMIN PANEL:
⦁ Login credentials
⦁ Admin Profile
⦁ Agent details to view
⦁ Block/unblock agent record
⦁ Password change
⦁ Bus details to be managed ( add/delete/edit/block/unblock/pagination)
⦁ Seat structure designed
⦁ Bus images and videos managed
⦁ Passenger details managed
⦁ Ticket details managed
⦁ Seat details managed
⦁ Ticket bookings managed
⦁ Cancellation managed
⦁ Payment details managed
⦁ View bus transaction details
⦁ Cancelling policies
⦁ SMS details managed
⦁ Email details managed
AGENT PANEL:
⦁ Login credentials
⦁ Control Panel managed
⦁ Agent Profile
⦁ Deposits
⦁ Target and Incentives managed
⦁ Booking reports managed
⦁ Ledger details managed
⦁ Monthly-yearly-today chart managed
⦁ Bus transaction reports managed
⦁ Wallet/print ticket/cancellation managed

ADMIN PANEL:
⦁ Login credentials
⦁ Tickets sold (day,month,year)
⦁ Ticket sale graph
⦁ City management
⦁ Manage the city details (edit, status, delete, pagination)
⦁ Route management
⦁ User management
⦁ Manage the users details(edit,delete,status)
⦁ Passengers management
⦁ Ticket details
⦁ Booker details
⦁ Seat details
⦁ Bus type management
⦁ Ticket booking managed
⦁ Ticket cancellation
⦁ Seat managed
⦁ Payment managed
⦁ Commission managed
⦁ Refund status
⦁ Cancelling Policy
⦁ Bus service details
⦁ Advantage of ticket booking
⦁ Bulk SMS managed
⦁ Sms log details
⦁ Email log details
⦁ Banners managed
⦁ Manage marquee text
⦁ Day-to-day offer coupon codes would be generated.
Check out products:
https://www.doditsolutions.com/yatra-clone/
http://scriptstore.in/product/yatra-clone/
http://phpreadymadescripts.com/yatra-clone.html